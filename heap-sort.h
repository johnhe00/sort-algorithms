#ifndef HEAP_SORT_H
#define HEAP_SORT_H

#include "stdio.h"
#include "swap.h"

int getParent(int index) {
	if(index <= 0) {
		return -1;
	}
	else {
		return (index - 1) / 2;
	}
}

int getLeft(int index, int last) {
	int left = 2 * index + 1;
	if(left > last) {
		return -1;
	}
	else {
		return left;
	}
}

int getRight(int index, int last) {
	int right = 2 * index + 2;
	if(right > last) {
		return -1;
	}
	else {
		return right;
	}
}

void heap_sort(int * array, int length) {
	for(int last = 0; last < length; last++) {
		int child = last;
		int parent = getParent(child);
		while(parent != -1) {
			if(array[child] > array[parent]) {
				swap(&array[child], &array[parent]);
				child = parent;
				parent = getParent(child);
			}
			else {
				break;
			}
		}
	}

	for(int last = length - 1; last > 0;) {
		swap(&array[0], &array[last--]);
		int current = 0;
		int left = getLeft(0, last);
		int right = getRight(0, last);

		while(1) {
			if(right != -1) {
				int min;
				int max;
				if(array[left] > array[right]) {
					max = left;
					min = right;
				}
				else {
					max = right;
					min = left;
				}

				if(array[current] < array[max]) {
					swap(&array[current], &array[max]);
					current = max;
				}
				else if(array[current] < array[min]) {
					swap(&array[current], &array[min]);
					current = min;
				}
				else {
					break;
				}

				left = getLeft(current, last);
				right = getRight(current, last);
			}
			else if(left != -1) {
				if(array[current] < array[left]) {
					swap(&array[current], &array[left]);
				}
				break;
			}
			else {
				break;
			}
		}
	}
}

#endif
