#ifndef COUNTING_SORT
#define COUNTING_SORT

#include "stdio.h"
#include "stdlib.h"

void counting_sort(int * array, int length) {
   int * barray = malloc(sizeof(int) * length);
   int max = 0;
   int min = 0;
   
   //Copies array a to array b//
   for(int i = 0; i < length; i++) {
      barray[i] = array[i];
      if(max < array[i]) {
         max = array[i];
      }
      if(min > array[i]) {
         min = array[i];
      }
   }
   
   //Initialize a counter array//
   int k = max - min + 1;
   int * count = malloc(sizeof(int) * k);
   for(int i = 0; i < k; i++) {
	count[i] = 0;
   }

   //Count the elemnts of a//
   for(int i = 0; i < length; i++) {
	count[array[i] - min]++;
   }

   //Stablize the count array//   
   for(int i = 1; i < k; i++) {
	count[i] += count[i-1];
   }

   //Reassign values to array a//
   for(int i = 0; i < length; i++) {
      count[barray[i] - min]--;
      array[count[barray[i] - min]] = barray[i];
   }
   
   free(count);
   free(barray);
}

#endif
