#ifndef MERGE_SORT
#define MERGE_SORT

#include "stdio.h"
#include "stdlib.h"

int * merge_sort(int * array, int start, int end) {
	int length = end - start + 1;
	int * ret = malloc(sizeof(int) * length);
	if(start == end) {
		ret[0] = array[start];
		return ret;
	}

	int middle = length / 2;
	int * first = merge_sort(array, start, start + middle - 1);
	int * second = merge_sort(array, start + middle, end);

	int s1 = 0;
	int s2 = 0;
	int e1 = middle;
	int e2 = end - start - middle + 1;

	for(int i = 0; i < length; i++) {
		if(s2 < e2 && s1 < e1) {
			if(first[s1] <= second[s2]) {
				ret[i] = first[s1++];
			}
			else {
				ret[i] = second[s2++];
			}
		}
		else if(s1 < e1) {
			ret[i] = first[s1++];
		}
		else if(s2 < e2) {
			ret[i] = second[s2++];
		}
	}
	free(first);
	free(second);

	return ret;
}

#endif
