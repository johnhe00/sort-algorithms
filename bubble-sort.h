#ifndef BUBBLE_SORT_H
#define BUBBLE_SORT_H

#include "stdio.h"
#include "stdbool.h"
#include "swap.h"

void bubble_sort(int * array, int length) {
	bool flag = true;
	int newl = 0;
	while(flag) {
		flag = false;
		for(int i = 0; i < length - 1; i++) {
			if(array[i] > array[i + 1]) {
				swap(&array[i], &array[i+1]);
				flag = true;
				newl = i + 1;
			}
		}
		length = newl;
	}
}

#endif
