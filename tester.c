#include "bubble-sort.h"
#include "counting-sort.h"
#include "heap-sort.h"
#include "merge-sort.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"

int validate(int * array, int length) {
	int val = 1;
	for(int i = 1; i < length; i++) {
		if(array[i] < array[i-1]) {
			val = 0;
			break;
		}
	}

	return val;
}

int main() {
	int length;
	scanf("%d", &length);

	int * array = malloc(sizeof(int) * length);
	for(int i = 0; i < length; i++) {
		scanf("%d", &array[i]);
	}
	clock_t start, end;
	double elapsed;

	printf("N = %d\n\n", length);

	start = clock();
        int * temp = merge_sort(array, 0, length - 1);
        end = clock();
        elapsed = (end - start) * 1000 / CLOCKS_PER_SEC;
        printf("Merge Sort\n");
	printf("Status: ");
	if(validate(temp, length)) {
		printf("Sorted\n");
	}
	else {
		printf("Not Sorted\n");
	}
        printf("Time: %.1lf milliseconds\n\n", elapsed);
        free(temp);
/*
	start = clock();
        bubble_sort(array, length);
        end = clock();
        elapsed = (end - start) * 1000 / CLOCKS_PER_SEC;
        printf("Bubble Sort\n");
	printf("Status: ");
        if(validate(array, length)) {
                printf("Sorted\n");
        }
        else {
                printf("Not Sorted\n");
        }
        printf("Time: %.1lf milliseconds\n\n", elapsed);
*/	
	start = clock();
        heap_sort(array, length);
        end = clock();
        elapsed = (end - start) * 1000 / CLOCKS_PER_SEC;
        printf("Heap Sort\n");
	printf("Status: ");
        if(validate(array, length)) {
                printf("Sorted\n");
        }
        else {
                printf("Not Sorted\n");
        }
        printf("Time: %.1lf milliseconds\n\n", elapsed);

	start = clock();
	counting_sort(array, length);
        end = clock();
        elapsed = (end - start) * 1000 / CLOCKS_PER_SEC;
	printf("Counting Sort\n");
	printf("Status: ");
        if(validate(array, length)) {
                printf("Sorted\n");
        }
        else {
                printf("Not Sorted\n");
        }
	printf("Time: %.1lf milliseconds\n\n", elapsed);

	free(array);

	return 0;
}
